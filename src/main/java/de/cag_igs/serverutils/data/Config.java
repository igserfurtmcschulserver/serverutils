package de.cag_igs.serverutils.data;


import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Config {

    private JavaPlugin plugin;
    private Map<String, Object> configValues;

    public Config(JavaPlugin plugin) {
        this.plugin = plugin;
        configValues = new HashMap<>();
    }

    public void loadConfig() {
        plugin.reloadConfig();
        plugin.getConfig().addDefault("broadcast.prefix", "&1[ServerUtils]&r");
        plugin.getConfig().addDefault("whitelistkick.prefix", "&1[ServerUtils]&r");
        plugin.getConfig().addDefault("util.databasetype", "sqlite");
        plugin.getConfig().addDefault("broadcaster.enable", true);
        plugin.getConfig().addDefault("broadcaster.prefix", "&1[ServerUtils]&r");
        plugin.getConfig().addDefault("broadcaster.maxtime", 120 );
        plugin.getConfig().addDefault("rulesaccept.position.x", 100.);
        plugin.getConfig().addDefault("rulesaccept.position.y", 100.);
        plugin.getConfig().addDefault("rulesaccept.position.z", 100.);
        plugin.getConfig().addDefault("rulesaccept.position.yaw", 1.f);
        plugin.getConfig().addDefault("rulesaccept.position.pitch", 1.f);
        plugin.getConfig().addDefault("rulesaccept.position.world", "World");
        plugin.getConfig().addDefault("rulesaccept.password", "regeln");
        plugin.getConfig().addDefault("rulesaccept.group.newbie", "default");
        plugin.getConfig().addDefault("rulesaccept.group.promote", "Spieler");
        plugin.getConfig().addDefault("rulesaccept.tp.enable", true);
        plugin.getConfig().addDefault("rulesaccept.ignoreCase", false);
        plugin.getConfig().addDefault("rulesaccept.enable", true);
        plugin.getConfig().addDefault("scoreboard.enable", true);
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();
        Set<String> keys = plugin.getConfig().getKeys(true);
        for (String key : keys) {
            configValues.put(key, plugin.getConfig().get(key));
        }
    }

    public Object getValue(String key) {
        return configValues.get(key);
    }

    public void setValue(String key, Object value) {
        configValues.put(key, value);
        plugin.getConfig().set(key, value);
        plugin.saveConfig();
    }
}
