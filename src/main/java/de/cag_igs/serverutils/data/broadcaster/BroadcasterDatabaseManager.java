package de.cag_igs.serverutils.data.broadcaster;

import de.cag_igs.serverutils.data.Config;
import org.bitbucket.igelborstel.igelcore.database.IDatabase;
import org.bitbucket.igelborstel.igelcore.database.MySql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class BroadcasterDatabaseManager {
    private IDatabase database;

    public BroadcasterDatabaseManager(IDatabase database) {
        this.database = database;
        createTables();
    }

    @SuppressWarnings("deprecated")
    private void createTables() {
        if (database instanceof MySql) {
            this.database.doUpdate("CREATE TABLE IF NOT EXISTS `serverutils_broadcasts` (id INT NOT NULL " +
                    "AUTO_INCREMENT PRIMARY KEY, intervall INT NOT NULL, message VARCHAR(200), activated " +
                    "BOOLEAN)");
        }
    }

    public void insertEntry(int time, String message, boolean active) {
        int activeNumber = (active) ? 1 : 0;
        this.database.doUpdate("INSERT INTO `serverutils_broadcasts`(`intervall`, `message`, `activated`) " +
                "VALUES (?, ?, ?)", Integer.toString(time), message, Integer.toString(activeNumber));
    }

    public BroadcastEntry getEntryById(int id) {
        ResultSet results = this.database.doQuery("SELECT intervall, message, activated " +
                "FROM `serverutils_broadcasts` WHERE id=?", Integer.toString(id)); //1 is true
        try
        {
            BroadcastEntry temp = null;
            while(results.next())
            {
                int intervall = results.getInt("intervall");
                String message = results.getString("message");
                boolean active = results.getBoolean("activated");
                temp = new BroadcastEntry(id, intervall, message, active);
            }
            return temp;
        } catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("deprecated")
    public Set<BroadcastEntry> getBroadcastEntries() {
        ResultSet results = this.database.doQuery("SELECT id, intervall, message, activated " +
                                                    "FROM `serverutils_broadcasts`");
        Set<BroadcastEntry> broadcastEntries = new HashSet<>();
        try
        {
            while(results.next())
            {
                int id = results.getInt("id");
                int intervall = results.getInt("intervall");
                String message = results.getString("message");
                boolean active = results.getBoolean("activated");
                BroadcastEntry temp = new BroadcastEntry(id, intervall, message, active);
                broadcastEntries.add(temp);
            }
            return broadcastEntries;
        } catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public Set<BroadcastEntry> getActiveBroadcastEntrys() {
        ResultSet results = this.database.doQuery("SELECT id, intervall, message, activated " +
                "FROM `serverutils_broadcasts` WHERE activated=?", Integer.toString(1)); //1 is true
        Set<BroadcastEntry> broadcastEntries = new HashSet<>();
        try {
            while (results.next())
            {
                int id = results.getInt("id");
                int intervall = results.getInt("intervall");
                String message = results.getString("message");
                boolean active = results.getBoolean("activated");
                BroadcastEntry temp = new BroadcastEntry(id, intervall, message, active);
                broadcastEntries.add(temp);
            }
            return broadcastEntries;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void deleteEntryById(int id) {
        this.database.doUpdate("DELETE FROM `serverutils_broadcasts` WHERE id=?", Integer.toString(id));
    }

    public boolean updateToggleStateById(int id) {
        ResultSet results = this.database.doQuery("SELECT activated FROM `serverutils_broadcasts` WHERE id=?",
                Integer.toString(id));
        boolean stat;
        try {
            stat = results.next() && results.getBoolean("activated");
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        if (stat) {
            this.database.doUpdate("UPDATE `serverutils_broadcasts` SET activated=? WHERE id=?",
                    Integer.toString(0), Integer.toString(id));
            return false;
        } else {
            this.database.doUpdate("UPDATE `serverutils_broadcasts` SET activated=? WHERE id=?",
                    Integer.toString(1), Integer.toString(id));
            return true;
        }
    }
}
