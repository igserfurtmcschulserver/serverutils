package de.cag_igs.serverutils.data.broadcaster;


public class BroadcastEntry {
    private int id;
    private int intervall;
    private String message;
    private boolean active;

    public BroadcastEntry(int id, int intervall, String message, boolean active) {
        this.id = id;
        this.intervall = intervall;
        this.message = message;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIntervall() {
        return intervall;
    }

    public void setIntervall(int intervall) {
        this.intervall = intervall;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
