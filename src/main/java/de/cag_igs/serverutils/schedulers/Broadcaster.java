package de.cag_igs.serverutils.schedulers;


import de.cag_igs.serverutils.data.Config;
import de.cag_igs.serverutils.data.broadcaster.BroadcastEntry;
import de.cag_igs.serverutils.data.broadcaster.BroadcasterDatabaseManager;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Broadcaster {

    private JavaPlugin plugin;
    private Config pluginConfig;
    private BroadcasterDatabaseManager databaseManager;
    private List<Integer> taskList;
    private Set<BroadcastEntry> broadcastEntries;

    public Broadcaster(JavaPlugin plugin, Config config, BroadcasterDatabaseManager broadcasterDatabaseManager) {
        this.plugin = plugin;
        this.pluginConfig = config;
        this.databaseManager = broadcasterDatabaseManager;

        this.taskList = new ArrayList<>();
        broadcastEntries = null;
    }

    public void reloadBroadcasts() {
        killTasks();
        clearData();
        loadBroadcasts();
    }

    private void clearData() {
        this.taskList.clear();
        if (broadcastEntries != null) {
            broadcastEntries.clear();
        }
    }

    private void loadBroadcasts() {
        broadcastEntries = this.databaseManager.getActiveBroadcastEntrys();
        String broadcastPrefix = (String) this.pluginConfig.getValue("broadcaster.prefix");
        for (BroadcastEntry entry : broadcastEntries) {
            this.taskList.add(this.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> plugin
                    .getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', broadcastPrefix
                            + entry.getMessage())) , 0L, entry.getIntervall() * 1200L));
        }
    }

    private void killTasks() {
        for (Integer taskid : taskList) {
            this.plugin.getServer().getScheduler().cancelTask(taskid);
        }
    }
}
