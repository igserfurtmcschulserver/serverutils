package de.cag_igs.serverutils.schedulers;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

/**
 * Created by Markus on 13.02.2017.
 */
public class Informationboard implements Runnable {
    private JavaPlugin plugin;
    private Scoreboard scoreboard;
    private Objective primaryObjective;

    public Informationboard(JavaPlugin plugin) {
        this.plugin = plugin;
        this.scoreboard = this.plugin.getServer().getScoreboardManager().getNewScoreboard();
        this.primaryObjective = this.scoreboard.registerNewObjective("Stats", "stats");
        this.primaryObjective.setDisplayName("IGS Server");
        this.primaryObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void run() {
        for (Player player : this.plugin.getServer().getOnlinePlayers()) {
            player.setScoreboard(scoreboard);
        }
        this.primaryObjective.getScore(ChatColor.DARK_GREEN + "Team: ").setScore(getTeamMembers());
        this.primaryObjective.getScore(ChatColor.AQUA + "Member: ").setScore(this.plugin.getServer()
                .getOnlinePlayers().size() - getTeamMembers());
    }

    public int getTeamMembers() {
        int teamMembers = 0;
        for (Player player : this.plugin.getServer().getOnlinePlayers()) {
            if (player.hasPermission("serverutils.scoreboard.mod")) {
                ++teamMembers;
            }
        }
        return teamMembers;
    }
}