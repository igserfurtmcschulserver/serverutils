package de.cag_igs.serverutils.commands;

import de.cag_igs.serverutils.data.Config;
import net.milkbowl.vault.permission.Permission;
import org.bitbucket.igelborstel.igelcore.commandHandlers.PlayerCommand;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;


public class AcceptCommand extends PlayerCommand {
    private JavaPlugin plugin;
    private Config config;

    private static Permission perms;

    public AcceptCommand(JavaPlugin pl, Config conf) {
        this.plugin = pl;
        this.config = conf;
        if (!setupPermisssions()) {
            throw new RuntimeException("Error during Permission Setup!");
        }
    }

    @Override
    public boolean command(Player player, Command command, String[] args) {
        if (args.length != 1) {
            player.sendMessage("Nur 1 Argument (Passwort) ist zum freischalten erlaubt!");
            return false;
        }
        if (!perms.playerInGroup(player, (String) config.getValue("rulesaccept.group.newbie"))) {
            player.sendMessage("Du kannst dich nur einmal freischalten!");
            return true;
        } else {
            boolean correctPassword;
            if ((boolean) config.getValue("rulesaccept.ignoreCase")) {
                correctPassword = args[0].equalsIgnoreCase((String) config.getValue("rulesaccept.password"));
            } else {
                correctPassword = args[0].equals(config.getValue("rulesaccept.password"));
            }

            if (correctPassword) {
                if ((boolean) config.getValue("rulesaccept.tp.enable")) {
                    Location teleportTarget = new Location(plugin.getServer().getWorld((String) config.getValue
                            ("rulesaccept.position.world")),
                            (double) config.getValue("rulesaccept.position.x"),
                            (double) config.getValue("rulesaccept.position.y"),
                            (double) config.getValue("rulesaccept.position.z"),
                            ((Double) config.getValue("rulesaccept.position.yaw")).floatValue(),
                            ((Double) config.getValue("rulesaccept.position.pitch")).floatValue());
                    player.teleport(teleportTarget);
                }
                perms.playerAddGroup(player, (String) config.getValue("rulesaccept.group.promote"));
                perms.playerRemoveGroup(player, (String) config.getValue("rulesaccept.group.newbie"));
                player.sendMessage("Du hast dich erfolgreich freigeschalten!");
                plugin.getServer().broadcastMessage(player.getDisplayName() + ChatColor.DARK_GREEN + " wurde " +
                        "erfolgreich zum Spieler!");
                return true;
            } else {
                player.sendMessage("Du hast das falsche Passwort eingegeben!");
                return true;
            }
        }
    }

    private boolean setupPermisssions() {
        RegisteredServiceProvider<Permission> rsp = this.plugin.getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
}
