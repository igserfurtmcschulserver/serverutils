package de.cag_igs.serverutils.commands;

import de.cag_igs.serverutils.data.Config;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;


public class BroadcastCommand implements CommandExecutor{

    private JavaPlugin plugin;
    private Config config;

    public BroadcastCommand(JavaPlugin plugin, Config conf) {
        this.plugin = plugin;
        this.config = conf;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("broadcast")) {
            if (sender.hasPermission("serverutils.broadcast")) {
                if (args.length > 0)
                {
                    String msg = "";
                    for (String arg : args) {
                        msg += arg + " ";
                    }
                    this.plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&',
                            (String) config.getValue("broadcast.prefix")) + ChatColor.translateAlternateColorCodes('&',
                            msg));
                    return true;
                } else {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                            (String) config.getValue("broadcast.prefix")) +
                            "Du musst für den Broadcastcommand eine Nachricht übergeben!");
                    return false;
                }
            } else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        (String) config.getValue("broadcast.prefix")) +
                        "Du hast nicht genügend Rechte um einen Broadcast zu senden!");
                return true;
            }
        }
        return false;
    }
}
