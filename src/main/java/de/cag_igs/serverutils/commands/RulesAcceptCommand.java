package de.cag_igs.serverutils.commands;


import de.cag_igs.serverutils.data.Config;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


public class RulesAcceptCommand implements CommandExecutor {

    private Config config;
    private JavaPlugin plugin;

    public RulesAcceptCommand(JavaPlugin plugin, Config config) {
        this.plugin = plugin;
        this.config = config;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (sender.hasPermission("serverutils.rulesaccept")) {
                sender.sendMessage("Parameter sind umbedingt erforderlich!");
                return false;
            } else {
                sender.sendMessage("Du darfst diesen Befehl nicht nutzen!");
                return true;
            }
        }

        if (command.getName().equalsIgnoreCase("rulesaccept")) {
            if (args[0].equalsIgnoreCase("tp")) {
                if (sender.hasPermission("serverutils.rulesaccept.tp")) {
                    if (sender instanceof Player) {
                        if (args.length == 1) {
                            Location teleportPosition = new Location(
                                    plugin.getServer().getWorld((String) config.getValue("rulesaccept.position.world")),
                                    (double) config.getValue("rulesaccept.position.x"),
                                    (double) config.getValue("rulesaccept.position.y"),
                                    (double) config.getValue("rulesaccept.position.z"),
                                    ((Double) config.getValue("rulesaccept.position.yaw")).floatValue(),
                                    ((Double) config.getValue("rulesaccept.position.pitch")).floatValue());
                            Player player = (Player) sender;
                            player.teleport(teleportPosition);
                            player.sendMessage("Du hast dich erfolgreich zur Freischaltteleportpunkt teleportiert!");
                            return true;
                        } else {
                            sender.sendMessage("Du darfst für das teleportieren keine Argument angeben!");
                            return false;
                        }
                    } else {
                        sender.sendMessage("Nur Spieler können sich zum Freischaltpunkt teleportieren.");
                        return true;
                    }
                } else {
                    sender.sendMessage("Du hast nicht die nötigen Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("settp")) {
                if (sender.hasPermission("serverutils.rulesaccept.set.tp")) {
                    if (sender instanceof Player) {
                        if (args.length == 1) {
                            Player player = (Player) sender;
                            Location currentLocation = player.getLocation();
                            config.setValue("rulesaccept.position.x", currentLocation.getX());
                            config.setValue("rulesaccept.position.y", currentLocation.getY());
                            config.setValue("rulesaccept.position.z", currentLocation.getZ());
                            config.setValue("rulesaccept.position.yaw", currentLocation.getYaw());
                            config.setValue("rulesaccept.position.pitch", currentLocation.getPitch());
                            config.setValue("rulesaccept.position.world", currentLocation.getWorld());
                            player.sendMessage("Du hast den Freischaltteleportpunkt erfolgreich gesetzt.");
                            return true;
                        } else {
                            sender.sendMessage("Du darfst für das setzen des Teleportpunkt kein Argument angeben");
                            return false;
                        }
                    } else {
                        sender.sendMessage("Nur Spieler können den Teleportpunkt setzen.");
                        return true;
                    }
                } else {
                    sender.sendMessage("Du hast nicht die nötigen Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("disabletp")) {
                if (sender.hasPermission("serverutils.rulesaccept.disable.tp")) {
                    if (args.length == 1) {
                        config.setValue("rulesaccept.tp.enable", false);
                        sender.sendMessage("Du hast das Teleportieren nach dem freischalten erfolgreich ausgeschalten");
                        return true;
                    } else {
                        sender.sendMessage("Du darfst kein Argument angeben!");
                        return false;
                    }
                } else {
                    sender.sendMessage("Du hast nicht die nötigen Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("enabletp")) {
                if (sender.hasPermission("serverutils.rulesaccept.enable.tp")) {
                    if (args.length == 1) {
                        config.setValue("rulesaccept.tp.enable", true);
                        sender.sendMessage("Du hast das Teleportieren nach dem freischalten erfolgreich eingeschalten");
                        return true;
                    } else {
                        sender.sendMessage("Du darfst kein Argument angeben!");
                        return false;
                    }
                } else {
                    sender.sendMessage("Du hast nicht die nötigen Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("ignorecase")) {
                if (sender.hasPermission("serverutils.rulesaccept.set.ignorecase")) {
                    if (args.length == 1) {
                        config.setValue("rulesaccept.ignoreCase", true);
                        sender.sendMessage("Du hast die Beachtung der Groß- und Kleinschreibung des Passworts " +
                                "ausgeschalten.");
                        return true;
                    } else {
                        sender.sendMessage("Du darfst kein Argument angeben!");
                        return false;
                    }
                } else {
                    sender.sendMessage("Du hast nicht die nötigen Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("disableignorecase")) {
                if (sender.hasPermission("serverutils.rulesaccept.set.ignorecase")) {
                    if (args.length == 1) {
                        config.setValue("rulesaccept.ignoreCase", false);
                        sender.sendMessage("Du hast die Beachtung der Groß- und Kleinschreibung des Passworts " +
                                "eingeschalten.");
                        return true;
                    } else {
                        sender.sendMessage("Du darfst kein Argument angeben!");
                        return false;
                    }
                } else {
                    sender.sendMessage("Du hast nicht die nötigen Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("getpassword")) {
                if (sender.hasPermission("serverutils.rulesaccept.get.password")) {
                    if (args.length == 1) {
                        sender.sendMessage("Das Passwort ist im Memoment: " + config.getValue("rulesaccept.password"));
                        return true;
                    } else {
                        sender.sendMessage("Du darfst keine Argumente angeben!");
                        return false;
                    }
                } else {
                    sender.sendMessage("Du hast nicht genügend Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("getnewbiegroup")) {
                if (sender.hasPermission("serverutils.rulesaccept.get.group.newbie")) {
                    if (args.length == 1) {
                        sender.sendMessage("Die Neulingsgruppe ist im Memoment: " + config.getValue("rulesaccept" +
                                ".group.newbie"));
                        return true;
                    } else {
                        sender.sendMessage("Du darfst keine Argumente angeben!");
                        return false;
                    }
                } else {
                    sender.sendMessage("Du hast nicht genügend Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("getpromotegroup")) {
                if (sender.hasPermission("serverutils.rulesaccept.get.group.promote")) {
                    if (args.length == 1) {
                        sender.sendMessage("Die Neulingsgruppe ist im Memoment: " + config.getValue("rulesaccept" +
                                ".group.promote"));
                        return true;
                    } else {
                        sender.sendMessage("Du darfst keine Argumente angeben!");
                        return false;
                    }
                } else {
                    sender.sendMessage("Du hast nicht genügend Rechte um dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("setpassword")) {
                if (sender.hasPermission("serverutils.rulesaccept.set.password")) {
                    if (args.length == 1) {
                        sender.sendMessage("Du musst mind. 1 Argument als Password angeben!");
                        return false;
                    } else {
                        sender.sendMessage("Du hast das Passwort auf " + args[1] + " gesetzt.");
                        config.setValue("rulesaccept.password", args[1]);
                        return true;
                    }
                } else {
                    sender.sendMessage("Du bist nicht berechtigt dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("setnewbiegroup")) {
                if (sender.hasPermission("serverutils.rulesaccept.set.group.newbie")) {
                    if (args.length == 1) {
                        sender.sendMessage("Du musst mind. 1 Argument als Newbiegruppe angeben!");
                        return false;
                    } else {
                        sender.sendMessage("Du hast die Newbiegruppe auf " + args[1] + " gesetzt.");
                        config.setValue("rulesaccept.group.newbie", args[1]);
                        return true;
                    }
                } else {
                    sender.sendMessage("Du bist nicht berechtigt dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("setpromotegroup")) {
                if (sender.hasPermission("serverutils.rulesaccept.set.group.promote")) {
                    if (args.length == 1) {
                        sender.sendMessage("Du musst mind. 1 Argument als Promotegruppe angeben!");
                        return false;
                    } else {
                        sender.sendMessage("Du hast die Promotegruppe auf " + args[1] + " gesetzt.");
                        config.setValue("rulesaccept.group.promote", args[1]);
                        return true;
                    }
                } else {
                    sender.sendMessage("Du bist nicht berechtigt dies zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("reload")) {
                if (sender.hasPermission("serverutils.rulesaccept.reload")) {
                    if (args.length == 1) {
                        config.loadConfig();
                        sender.sendMessage("Du hast Rulesaccept von der Config neu geladen!");
                        return true;
                    } else {
                        sender.sendMessage("Du darfst keine Parameter angeben!");
                        return false;
                    }
                } else {
                    sender.sendMessage("Du hast nicht die nötigen Rechte dafür");
                    return true;
                }
            } else {
                if (sender.hasPermission("serverutils.rulesaccept")) {
                    sender.sendMessage("Dieser Befehl ist unbekannt!");
                    return false;
                } else {
                    sender.sendMessage("Du hast nicht die notwendigen Rechte!");
                    return true;
                }
            }
        }
        return false;
    }
}