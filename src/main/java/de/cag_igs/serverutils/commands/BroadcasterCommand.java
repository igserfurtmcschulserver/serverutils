package de.cag_igs.serverutils.commands;

import de.cag_igs.serverutils.data.Config;
import de.cag_igs.serverutils.data.broadcaster.BroadcastEntry;
import de.cag_igs.serverutils.data.broadcaster.BroadcasterDatabaseManager;
import de.cag_igs.serverutils.schedulers.Broadcaster;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Set;


public class BroadcasterCommand implements CommandExecutor {

    private JavaPlugin plugin;
    private Config config;
    private BroadcasterDatabaseManager broadcasterDatabaseManager;
    private Broadcaster broadcaster;

    public BroadcasterCommand(JavaPlugin plugin, Config config, BroadcasterDatabaseManager broadcasterDatabaseManager,
                              Broadcaster broadcaster) {
        this.plugin = plugin;
        this.config = config;
        this.broadcasterDatabaseManager = broadcasterDatabaseManager;
        this.broadcaster = broadcaster;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("broadcaster")) {
            if (args.length < 1) {
                sender.sendMessage("Der Broadcaster wird anders bedient!");
                return false;
            }
            if (args[0].equalsIgnoreCase("new") || args[0].equalsIgnoreCase("add")) {
                if (sender.hasPermission("serverutils.broadcaster.add")) {
                    if (args.length < 3) {
                        sender.sendMessage("Zu wenig Argumente für einen neuen Broadcast angegeben!");
                        return false;
                    }
                    int interval;
                    try {
                        interval = Integer.parseInt(args[1]);
                    } catch (NumberFormatException exception) {
                        sender.sendMessage("Das zweite Argument muss eine Zahl sein!");
                        return false;
                    }
                    if (interval < 1 || interval > (int) config.getValue("broadcaster.maxtime")) {
                        sender.sendMessage("Das Interval muss im Bereich 1 < interval < " + Integer.toString((int)
                                config.getValue("broadcaster.maxtime")) + " liegen!");
                        return false;
                    }
                    StringBuilder msgBuilder = new StringBuilder();
                    for(int i = 0; i < args.length; ++i) {
                        if(i < 2) {
                            continue;
                        }
                        msgBuilder.append(args[i]);
                        msgBuilder.append(' ');
                    }
                    String msg = msgBuilder.toString();
                    if(msg.length() > 200) {
                        sender.sendMessage("Die Nachricht darf maximal 200 Zeichen lang sein");
                        return false;
                    }
                    this.broadcasterDatabaseManager.insertEntry(interval, msg, true);
                    sender.sendMessage("Du hast den Broadcast " + msg + " erfolgreich zur Datenbank hinzugefügt!");
                    return true;
                } else {
                    sender.sendMessage("Du hast nicht genügend Rechte um das zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("delete")) {
                if (sender.hasPermission("serverutils.broadcaster.remove")) {
                    if (args.length != 2) {
                        sender.sendMessage("Du darfst beim Löschen eines Broadcasts nur die ID angeben!");
                        return false;
                    }
                    int id;
                    try {
                        id = Integer.parseInt(args[1]);
                    } catch (NumberFormatException exception) {
                        sender.sendMessage("Die ID muss eine Zahl sein!");
                        return false;
                    }
                    this.broadcasterDatabaseManager.deleteEntryById(id);
                    sender.sendMessage("Du hast den Broadcast mit der ID " + Integer.toString(id) + "erfolgreich aus " +
                            "der Datenbank gelöscht!");
                    return true;
                } else {
                    sender.sendMessage("Du hast nicht genügend Rechte um das zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("toggle")) {
                if (sender.hasPermission("serverutils.broadcaster.toggle")) {
                    if (args.length != 2) {
                        sender.sendMessage("Du darfst Statusändern eines Broadcasts nur die ID angeben!");
                        return false;
                    }
                    int id;
                    try {
                        id = Integer.parseInt(args[1]);
                    } catch (NumberFormatException exception) {
                        sender.sendMessage("Die ID muss eine Zahl sein!");
                        return false;
                    }
                    sender.sendMessage("Du hast den Status des Broadcast " + Integer.toString(id) + " erfolgreich auf" +
                            " " + Boolean.toString(this.broadcasterDatabaseManager.updateToggleStateById(id)) +
                            " geändert!");
                    return true;
                } else {
                    sender.sendMessage("Du hast nicht genügend Rechte um das zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("reload")) {
                if (sender.hasPermission("serverutils.broadcaster.reload")) {
                    if (args.length != 1) {
                        sender.sendMessage("Beim Neuladen des Broadcast darfst du keine weiteren Argumente übergeben.");
                        return false;
                    }
                    this.broadcaster.reloadBroadcasts();
                    sender.sendMessage("Der Broadcaster wurde neugeladen!");
                    return true;
                } else {
                    sender.sendMessage("Du hast nicht genügend Rechte um das zu tun!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                if (sender.hasPermission("serverutils.broadcaster.list")) {
                    //TODO: Replace with a better (and tested) version
                    Set<BroadcastEntry> entrySet = broadcasterDatabaseManager.getBroadcastEntries();
                    sender.sendMessage("ID) [time] [activated] [Message]");
                    for (BroadcastEntry entry : entrySet) {
                        sender.sendMessage(entry.getId() + ") [" + entry.getIntervall() + "] [" + entry.isActive() +
                                "] " + entry.getMessage());
                    }
                    return true;
                } else {
                    sender.sendMessage("Du hast nicht genügend Rechte um das zu tun!");
                    return true;
                }
            } else {
                sender.sendMessage("Der Broadcaster wird anders bedient!");
                return false;
            }
        } else {
            return false;
        }
    }
}
