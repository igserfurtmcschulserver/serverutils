package de.cag_igs.serverutils.commands;

import de.cag_igs.serverutils.data.Config;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


public class WhitelistKickCommand implements CommandExecutor {

    private JavaPlugin plugin;
    private Config config;

    public WhitelistKickCommand(JavaPlugin parent, Config conf) {
        this.plugin = parent;
        this.config = conf;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("whitelistkick")) {
            Player executingPlayer = null;
            if (sender instanceof Player) {
                executingPlayer = (Player) sender;
            }
            if (sender.hasPermission("serverutils.whitelistkick")) {
                if (args.length == 0) {
                    for (Player player : plugin.getServer().getOnlinePlayers()) {
                        if (!player.hasPermission("serverutils.whitelistkick.ignore") &&
                                !player.isOp() &&
                                !player.isWhitelisted() &&
                                !(player == executingPlayer)) {
                            player.kickPlayer("Du wurdest vom Server geworfen, weil du nicht gewhitelisted bist");
                        }
                    }
                    plugin.getServer().broadcast(ChatColor.translateAlternateColorCodes('&',
                            (String) config.getValue("whitelistkick.prefix")) +
                            "Alle Spieler, die nicht auf der Whitelist standen wurdem vom Server geworfen",
                            "serverutils.whitelistkick.notify");
                    return true;
                } else {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                            (String) config.getValue("whitelistkick.prefix")) + "Du darfst keine Paramter angeben!");
                    return false;
                }
            } else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        (String) config.getValue("whitelistkick.prefix")) + "Du hast nicht die nötigen Rechte um alle " +
                        "Spieler vom Server zu werfen, die nicht auf der Whitelist stehen!");
                return true;
            }
        }
        return false;
    }
}
