package de.cag_igs.serverutils;

import de.cag_igs.serverutils.commands.*;
import de.cag_igs.serverutils.data.Config;
import de.cag_igs.serverutils.data.broadcaster.BroadcasterDatabaseManager;
import de.cag_igs.serverutils.schedulers.Broadcaster;
import de.cag_igs.serverutils.schedulers.Informationboard;
import org.bitbucket.igelborstel.igelcore.database.IDatabase;
import org.bitbucket.igelborstel.igelcore.database.MySql;
import org.bitbucket.igelborstel.igelcore.database.SqLite;
import org.bukkit.plugin.java.JavaPlugin;

public class ServerUtils extends JavaPlugin {

    private Config config;
    private IDatabase database;
    private BroadcasterDatabaseManager broadcasterDatabaseManager;
    private Broadcaster broadcaster;

    private boolean overwriteBroadcasterEnable;

    public ServerUtils() {
        super();
        config = new Config(this);
        overwriteBroadcasterEnable = false;
    }

    @Override
    public void onDisable() {
        super.onDisable();
        if (database.isConnected()) {
            database.close();
        }
    }

    @Override
    public void onEnable() {
        super.onEnable();
        config.loadConfig();
        String databasetype = (String) config.getValue("util.databasetype");
        if (databasetype.equalsIgnoreCase("mysql")) {
            database = new MySql(this.getDescription().getName());
            overwriteBroadcasterEnable = !database.connect();
        } else if (databasetype.equalsIgnoreCase("sqlite")) {
            database = new SqLite(this.getDescription().getName());
            overwriteBroadcasterEnable = !database.connect();
        } else {
            this.getLogger().warning("Databasetype not supported!");
            this.getLogger().warning("Broadcaster not activated!");
            overwriteBroadcasterEnable = true;
        }

        if (!overwriteBroadcasterEnable && (boolean) config.getValue("broadcaster.enable")) {
            broadcasterDatabaseManager = new BroadcasterDatabaseManager(database);
            broadcaster = new Broadcaster(this, config, broadcasterDatabaseManager);
            this.getCommand("broadcaster").setExecutor(new BroadcasterCommand(this, config,
                    broadcasterDatabaseManager, broadcaster));
            broadcaster.reloadBroadcasts();
        }

        if ((boolean) config.getValue("rulesaccept.enable")
                && this.getServer().getPluginManager().isPluginEnabled("Vault")) {
            this.getCommand("rulesaccept").setExecutor(new RulesAcceptCommand(this, config));
            this.getCommand("accept").setExecutor(new AcceptCommand(this, config));
        }

        if ((boolean) config.getValue("scoreboard.enable")) {
            this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Informationboard(this), 0L, 100L);
        }

        this.getCommand("broadcast").setExecutor(new BroadcastCommand(this, config));
        this.getCommand("whitelistkick").setExecutor(new WhitelistKickCommand(this, config));
    }
}
