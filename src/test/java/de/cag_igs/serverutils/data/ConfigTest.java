package de.cag_igs.serverutils.data;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JavaPlugin.class)
public class ConfigTest {

    private JavaPlugin mockPlugin;
    private Config config;
    private FileConfiguration mockFileConfiguration;
    private FileConfigurationOptions mockFileConfigurationOptions;
    private Set<String> keyList;

    @Before
    public void setUp() {
        mockPlugin = PowerMockito.mock(JavaPlugin.class);
        mockFileConfiguration = mock(FileConfiguration.class);
        mockFileConfigurationOptions = mock(FileConfigurationOptions.class);
        config = new Config(mockPlugin);
        when(mockPlugin.getConfig()).thenReturn(mockFileConfiguration);
        when(mockFileConfiguration.options()).thenReturn(mockFileConfigurationOptions);
        keyList = new HashSet<>();
        keyList.add("keya");
        keyList.add("keyb");
        keyList.add("complex.keya");
        keyList.add("complex.keyb");
        keyList.add("very.complex.keya");
        keyList.add("very.complex.keyb");
        doReturn(keyList).when(mockFileConfiguration).getKeys(anyBoolean());
    }

    @Test
    public void testLoadConfig() {
        config.loadConfig();
        verify(mockPlugin).reloadConfig();
        verify(mockFileConfiguration, atLeastOnce()).addDefault(anyString(), anyObject());
        verify(mockFileConfiguration).getKeys(eq(true));
        for (String str : keyList) {
            verify(mockFileConfiguration).get(eq(str));
        }
        //TODO: find a way to test data is added to map
        verify(mockFileConfigurationOptions).copyDefaults(eq(true));
        verify(mockPlugin).saveConfig();
    }

    @Test
    public void testGetValue() {
        //TODO: find a way to test the private map
    }

    @Test
    public void testSetValue() {
        config.setValue("broadcast.prefix", "&1[ServerUtils]&r");
        verify(mockFileConfiguration).set(anyString(), anyObject());
        verify(mockPlugin).saveConfig();
        //TODO: find a way to test the private map
    }

}