package de.cag_igs.serverutils.commands;

import de.cag_igs.serverutils.data.Config;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JavaPlugin.class)
public class WhitelistKickCommandTest {

    private Command mockCommand;
    private CommandSender mockCommandSender;
    private JavaPlugin mockPlugin;
    private static final String COMMAND_LABEL = "whitelistkick";
    private Server mockServer;
    private Player mockedExecutingPlayer;
    private Player mockedWhitelistedPlayer;
    private Player mockedIgnoredPlayer;
    private Player mockedOPPlayer;
    private WhitelistKickCommand whitelistKickCommand;
    private Player mockedSimplePlayer;
    private List<Player> playerList;
    private Config mockConfig;
    private String PREFIX = "&1[ServerUtils]&r";

    @Before
    public void setUp() {
        mockCommand = mock(Command.class);
        mockCommandSender = mock(CommandSender.class);

        mockPlugin = PowerMockito.mock(JavaPlugin.class);
        mockServer = mock(Server.class);

        mockedExecutingPlayer = mock(Player.class);
        mockedWhitelistedPlayer = mock(Player.class);
        mockedIgnoredPlayer = mock(Player.class);
        mockedOPPlayer = mock(Player.class);
        mockedSimplePlayer = mock(Player.class);
        mockConfig = mock(Config.class);
        whitelistKickCommand = new WhitelistKickCommand(mockPlugin, mockConfig);
        when(mockConfig.getValue(eq("whitelistkick.prefix"))).thenReturn(PREFIX);
        //when(mockCommand.getName()).thenReturn(commandLabel);
        when(mockPlugin.getServer()).thenReturn(mockServer);
        when(mockCommandSender.hasPermission("serverutils.whitelistkick")).thenReturn(true);
        when(mockedWhitelistedPlayer.isWhitelisted()).thenReturn(true);
        when(mockedIgnoredPlayer.hasPermission("serverutils.whitelistkick.ignore")).thenReturn(true);
        when(mockedOPPlayer.isOp()).thenReturn(true);
        when(mockedSimplePlayer.isWhitelisted()).thenReturn(false);
        when(mockedSimplePlayer.hasPermission("serverutils.whitelistkick.ignore")).thenReturn(false);
        when(mockedSimplePlayer.isOp()).thenReturn(false);
        playerList = new ArrayList<>();
        playerList.add(mockedWhitelistedPlayer);
        playerList.add(mockedExecutingPlayer);
        playerList.add(mockedIgnoredPlayer);
        playerList.add(mockedOPPlayer);
        playerList.add(mockedSimplePlayer);
        doReturn(playerList).when(mockServer).getOnlinePlayers();

    }

    @Test
    public void testOnCommandTestCase1() {
        when(mockedExecutingPlayer.hasPermission("serverutils.whitelistkick")).thenReturn(true);
        when(mockedExecutingPlayer.hasPermission("serverutils.whitelistkick.ignore")).thenReturn(true);
        String args[] = {};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(args) + " ",
                whitelistKickCommand.onCommand(mockCommandSender, mockCommand, COMMAND_LABEL, args));
        assertTrue("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(args) + " ",
                whitelistKickCommand.onCommand(mockedExecutingPlayer, mockCommand, COMMAND_LABEL, args));
        verify(mockServer, times(2)).broadcast(anyString(), eq("serverutils.whitelistkick.notify"));
        verify(mockCommandSender, never()).sendMessage(anyString());
        verify(mockedExecutingPlayer, never()).sendMessage(anyString());
        verify(mockedIgnoredPlayer, never()).kickPlayer(anyString());
        verify(mockedExecutingPlayer, never()).kickPlayer(anyString());
        verify(mockedWhitelistedPlayer, never()).kickPlayer(anyString());
        verify(mockedOPPlayer, never()).kickPlayer(anyString());
        verify(mockedSimplePlayer, times(2)).kickPlayer(anyString());
        verify(mockConfig, times(2)).getValue(eq("whitelistkick.prefix"));
    }

    @Test
    public void testOnCommandTestCase2() {
        when(mockedExecutingPlayer.hasPermission("serverutils.whitelistkick")).thenReturn(true);
        when(mockedExecutingPlayer.isWhitelisted()).thenReturn(false);
        when(mockedExecutingPlayer.hasPermission("serverutils.whitelistkick.ignore")).thenReturn(false);
        when(mockedExecutingPlayer.isOp()).thenReturn(false);
        String args[] = {};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(args) + " ",
                whitelistKickCommand.onCommand(mockedExecutingPlayer, mockCommand, COMMAND_LABEL, args));
        verify(mockServer).broadcast(anyString(), eq("serverutils.whitelistkick.notify"));
        verify(mockedExecutingPlayer, never()).sendMessage(anyString());
        verify(mockedIgnoredPlayer, never()).kickPlayer(anyString());
        verify(mockedExecutingPlayer, never()).kickPlayer(anyString());
        verify(mockedWhitelistedPlayer, never()).kickPlayer(anyString());
        verify(mockedOPPlayer, never()).kickPlayer(anyString());
        verify(mockedSimplePlayer).kickPlayer(anyString());
        verify(mockConfig).getValue(eq("whitelistkick.prefix"));
    }

    @Test
    public void testOnCommandTestCase3() {
        when(mockedExecutingPlayer.hasPermission("serverutils.whitelistkick")).thenReturn(true);
        String args[] = {"This", "is", "a", "simple", "arg-list."};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertFalse("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(args) + " ",
                whitelistKickCommand.onCommand(mockCommandSender, mockCommand, COMMAND_LABEL, args));
        assertFalse("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(args) + " ",
                whitelistKickCommand.onCommand(mockedExecutingPlayer, mockCommand, COMMAND_LABEL, args));
        verify(mockServer, never()).broadcast(anyString(), eq("serverutils.whitelistkick.notify"));
        verify(mockCommandSender).sendMessage(anyString());
        verify(mockedExecutingPlayer).sendMessage(anyString());
        verify(mockedIgnoredPlayer, never()).kickPlayer(anyString());
        verify(mockedExecutingPlayer, never()).kickPlayer(anyString());
        verify(mockedWhitelistedPlayer, never()).kickPlayer(anyString());
        verify(mockedOPPlayer, never()).kickPlayer(anyString());
        verify(mockedSimplePlayer, never()).kickPlayer(anyString());
        verify(mockConfig, times(2)).getValue(eq("whitelistkick.prefix"));
    }

    @Test
    public void testOnCommandTestCase4() {
        when(mockedExecutingPlayer.hasPermission("serverutils.whitelistkick")).thenReturn(false);
        String args[] = {};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(args) + " ",
                whitelistKickCommand.onCommand(mockedExecutingPlayer, mockCommand, COMMAND_LABEL, args));
        verify(mockServer, never()).broadcast(anyString(), eq("serverutils.whitelistkick.notify"));
        verify(mockedExecutingPlayer).sendMessage(anyString());
        verify(mockedIgnoredPlayer, never()).kickPlayer(anyString());
        verify(mockedExecutingPlayer, never()).kickPlayer(anyString());
        verify(mockedWhitelistedPlayer, never()).kickPlayer(anyString());
        verify(mockedOPPlayer, never()).kickPlayer(anyString());
        verify(mockedSimplePlayer, never()).kickPlayer(anyString());
        verify(mockConfig).getValue(eq("whitelistkick.prefix"));
    }

    @Test
    public void testOnCommandTestCase5() {
        when(mockedExecutingPlayer.hasPermission("serverutils.whitelistkick")).thenReturn(false);
        String args[] = {"This", "is", "a", "simple", "arg-list."};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(args) + " ",
                whitelistKickCommand.onCommand(mockedExecutingPlayer, mockCommand, COMMAND_LABEL, args));
        verify(mockServer, never()).broadcast(anyString(), eq("serverutils.whitelistkick.notify"));
        verify(mockedExecutingPlayer).sendMessage(anyString());
        verify(mockedIgnoredPlayer, never()).kickPlayer(anyString());
        verify(mockedExecutingPlayer, never()).kickPlayer(anyString());
        verify(mockedWhitelistedPlayer, never()).kickPlayer(anyString());
        verify(mockedOPPlayer, never()).kickPlayer(anyString());
        verify(mockedSimplePlayer, never()).kickPlayer(anyString());
        verify(mockConfig).getValue(eq("whitelistkick.prefix"));

    }
}