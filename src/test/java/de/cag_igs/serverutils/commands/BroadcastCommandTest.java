package de.cag_igs.serverutils.commands;


import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;

import de.cag_igs.serverutils.data.Config;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.util.Arrays;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JavaPlugin.class)
public class BroadcastCommandTest {
    private Command mockCommand;
    private CommandSender mockCommandSender;
    private JavaPlugin mockPlugin;
    private static final String COMMAND_LABEL = "broadcast";
    private Server mockServer;
    private BroadcastCommand broadcastCommand;
    private Config mockConfig;
    private static final String PREFIX = "&1[ServerUtils]&r";

    @Before
    public void setUp() {
        mockCommand = mock(Command.class);
        mockCommandSender = mock(CommandSender.class);
        mockPlugin = PowerMockito.mock(JavaPlugin.class);
        mockServer = mock(Server.class);
        mockConfig = mock(Config.class);
        when(mockPlugin.getServer()).thenReturn(mockServer);
        when(mockConfig.getValue(eq("broadcast.prefix"))).thenReturn(PREFIX);
        broadcastCommand = new BroadcastCommand(mockPlugin, mockConfig);
    }

    @Test
    public void testOnCommandTestCase1() {
        //test case 1: enough rights, valid message
        when(mockCommandSender.hasPermission("serverutils.broadcast")).thenReturn(true);
        String message[] = {"This",  "is", "a", "simple", "message"};
        int i = 0;
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(message) + " ",
                broadcastCommand.onCommand(mockCommandSender, mockCommand, COMMAND_LABEL, message));
        verify(mockServer).broadcastMessage(anyString());
        verify(mockCommandSender, never()).sendMessage(anyString());
        verify(mockConfig).getValue(eq("broadcast.prefix"));
    }

    @Test
    public void testOnCommandTestCase2() {
        //test case 1: enough rights, valid message
        when(mockCommandSender.hasPermission("serverutils.broadcast")).thenReturn(true);
        String[] message = new String[]{};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertFalse("Unvalid command /" + COMMAND_LABEL + " " + Arrays.toString(message) + " ",
                broadcastCommand.onCommand(mockCommandSender, mockCommand, COMMAND_LABEL, message));
        verify(mockServer, never()).broadcastMessage(anyString());
        verify(mockCommandSender).sendMessage(anyString());
        verify(mockConfig).getValue(eq("broadcast.prefix"));
    }

    @Test
    public void testOnCommandTestCase3() {
        //test case 1: enough rights, valid message
        when(mockCommandSender.hasPermission("serverutils.broadcast")).thenReturn(false);
        String[] message = new String[]{"This",  "is", "a", "simple", "message"};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(message) + " ",
                broadcastCommand.onCommand(mockCommandSender, mockCommand, COMMAND_LABEL, message));
        verify(mockServer, never()).broadcastMessage(anyString());
        verify(mockCommandSender).sendMessage(anyString());
        verify(mockConfig).getValue(eq("broadcast.prefix"));
    }

    @Test
    public void testOnCommandTestCase4() {
        //test case 1: enough rights, valid message
        when(mockCommandSender.hasPermission("serverutils.broadcast")).thenReturn(false);
        String[] message = new String[]{};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue("Valid command /" + COMMAND_LABEL + " " + Arrays.toString(message) + " ",
                broadcastCommand.onCommand(mockCommandSender, mockCommand, COMMAND_LABEL, message));
        verify(mockServer, never()).broadcastMessage(anyString());
        verify(mockCommandSender).sendMessage(anyString());
        verify(mockConfig).getValue(eq("broadcast.prefix"));
    }
}
