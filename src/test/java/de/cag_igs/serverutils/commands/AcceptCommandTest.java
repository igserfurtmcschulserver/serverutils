package de.cag_igs.serverutils.commands;

import de.cag_igs.serverutils.data.Config;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JavaPlugin.class)
public class AcceptCommandTest {

    private Command mockCommand;
    private CommandSender mockCommandSender;
    private JavaPlugin mockPlugin;
    private static final String commandLabel = "accept";
    private Server mockServer;
    private AcceptCommand acceptCommand;
    private static final String CORRECTPASSWORD = "AtdHo";
    private String[] passwordCombinations;
    private Player mockPlayer;
    private Config mockConfig;
    private Location teleportLocation;
    private Permission mockPermission;
    private ServicesManager mockServicesManager;
    private RegisteredServiceProvider<Permission> mockRegisteredServiceProvider;
    private World mockWorld;
    @Before
    public void setUp() {
        mockCommand = mock(Command.class);
        when(mockCommand.getName()).thenReturn(commandLabel);
        when(mockCommand.getLabel()).thenReturn(commandLabel);
        mockCommandSender = mock(CommandSender.class);
        mockPlugin = PowerMockito.mock(JavaPlugin.class);
        mockServer = mock(Server.class);
        when(mockPlugin.getServer()).thenReturn(mockServer);
        mockServicesManager = mock(ServicesManager.class);
        when(mockServer.getServicesManager()).thenReturn(mockServicesManager);
        mockRegisteredServiceProvider = mock(RegisteredServiceProvider.class);
        when(mockServicesManager.getRegistration(Permission.class)).thenReturn(mockRegisteredServiceProvider);
        mockPermission = mock(Permission.class);
        when(mockRegisteredServiceProvider.getProvider()).thenReturn(mockPermission);
        mockConfig = mock(Config.class);
        when(mockConfig.getValue("rulesaccept.group.newbie")).thenReturn("newbie");
        when(mockConfig.getValue("rulesaccept.group.promote")).thenReturn("promote");
        when(mockConfig.getValue("rulesaccept.position.x")).thenReturn(100.);
        when(mockConfig.getValue("rulesaccept.position.y")).thenReturn(100.);
        when(mockConfig.getValue("rulesaccept.position.z")).thenReturn(100.);
        when(mockConfig.getValue("rulesaccept.position.yaw")).thenReturn(1.);
        when(mockConfig.getValue("rulesaccept.position.pitch")).thenReturn(1.);
        when(mockConfig.getValue("rulesaccept.position.world")).thenReturn("World");
        when(mockConfig.getValue("rulesaccept.password")).thenReturn(CORRECTPASSWORD);
        mockWorld = mock(World.class);
        when(mockServer.getWorld(eq("World"))).thenReturn(mockWorld);
        teleportLocation = new Location(mockWorld, 100., 100., 100., 1.f, 1.f);
        //when(mockPlugin.getServer()).thenReturn(mockServer);
        acceptCommand = new AcceptCommand(mockPlugin, mockConfig);
        passwordCombinations = permute(CORRECTPASSWORD);
        mockPlayer = mock(Player.class);

        passwordCombinations = permute(CORRECTPASSWORD);

    }

    private String[] permute(String s){
        String[] returnArray;
        if(s.length() == 1){
            returnArray = new String[2];
            returnArray[0] = s.toUpperCase();
            returnArray[1] = s.toLowerCase();
            return returnArray;
        }
        String[] permutedArray = permute(s.substring(1));
        returnArray = new String[permutedArray.length*2];
        for(int i = 0; i < permutedArray.length; i++){
            returnArray[i*2] = s.substring(0,1).toUpperCase()+permutedArray[i];
            returnArray[i*2+1] = s.substring(0,1).toLowerCase()+permutedArray[i];
        }
        return returnArray;
    }


    @Test
    public void testOnCommandCasePlayerAlreadyRegistered() {
        //player is already registered
        when(mockPermission.playerInGroup(eq(mockPlayer), eq("newbie"))).thenReturn(false);
        assertTrue(acceptCommand.command(mockPlayer, mockCommand, new String[] {"foobar"}));
        verify(mockPlayer, never()).teleport(any(Location.class));
        verify(mockPermission, never()).playerAddGroup(eq(mockPlayer), eq("promote"));
        verify(mockPermission, never()).playerRemoveGroup(eq(mockPlayer), eq("newbie"));
    }

    @Test
    public void testOnCommandCasePlayerEnterCorrectPasswordIgnoreCase() {
        when(mockConfig.getValue("rulesaccept.ignoreCase")).thenReturn(true);
        when(mockConfig.getValue("rulesaccept.tp.enable")).thenReturn(true);
        when(mockPermission.playerInGroup(eq(mockPlayer), eq("newbie"))).thenReturn(true);
        for (String pwd : passwordCombinations) {
            assertTrue(acceptCommand.command(mockPlayer, mockCommand, new String[] { pwd }));
            assertFalse(acceptCommand.command(mockPlayer, mockCommand, new String[] { pwd, "foobar"}));
        }
        verify(mockPlayer, times(passwordCombinations.length)).teleport(eq(teleportLocation));
        verify(mockPermission, times(passwordCombinations.length)).playerAddGroup(eq(mockPlayer), eq("promote"));
        verify(mockPermission, times(passwordCombinations.length)).playerRemoveGroup(eq(mockPlayer), eq("newbie"));
    }

    @Test
    public void testOnCommandCasePlayerEnterCorrectPasswordCaseSensitve() {
        when(mockConfig.getValue("rulesaccept.ignoreCase")).thenReturn(false);
        when(mockConfig.getValue("rulesaccept.tp.enable")).thenReturn(true);
        when(mockPermission.playerInGroup(eq(mockPlayer), eq("newbie"))).thenReturn(true);
        for (String pwd : passwordCombinations) {
            assertTrue(acceptCommand.command(mockPlayer, mockCommand, new String[] { pwd }));
            assertFalse(acceptCommand.command(mockPlayer, mockCommand, new String[] { pwd, "foobar"}));
        }
        verify(mockPlayer, times(1)).teleport(eq(teleportLocation));
        verify(mockPermission, times(1)).playerAddGroup(eq(mockPlayer), eq("promote"));
        verify(mockPermission, times(1)).playerRemoveGroup(eq(mockPlayer), eq("newbie"));
    }

    @Test
    public void testOnCommandCasePlayerEnterCorrectPasswordIgnoreCaseTeleportDisabled() {
        when(mockConfig.getValue("rulesaccept.ignoreCase")).thenReturn(true);
        when(mockConfig.getValue("rulesaccept.tp.enable")).thenReturn(false);
        when(mockPermission.playerInGroup(eq(mockPlayer), eq("newbie"))).thenReturn(true);
        for (String pwd : passwordCombinations) {
            assertTrue(acceptCommand.command(mockPlayer, mockCommand, new String[] { pwd }));
            assertFalse(acceptCommand.command(mockPlayer, mockCommand, new String[] { pwd, "foobar"}));
        }
        verify(mockPlayer, never()).teleport(any(Location.class));
        verify(mockPermission, times(passwordCombinations.length)).playerAddGroup(eq(mockPlayer), eq("promote"));
        verify(mockPermission, times(passwordCombinations.length)).playerRemoveGroup(eq(mockPlayer), eq("newbie"));
    }

    @Test
    public void testOnCommandCasePlayerEnterNoPassword() {
        when(mockPermission.playerInGroup(eq(mockPlayer), eq("newbie"))).thenReturn(true);
        assertFalse(acceptCommand.command(mockPlayer, mockCommand, new String[] {}));
        verify(mockPlayer, never()).teleport(any(Location.class));
        verify(mockPermission, never()).playerAddGroup(eq(mockPlayer), eq("promote"));
        verify(mockPermission, never()).playerRemoveGroup(eq(mockPlayer), eq("newbie"));
    }
}