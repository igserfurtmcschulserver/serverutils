package de.cag_igs.serverutils.commands;

import de.cag_igs.serverutils.data.Config;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JavaPlugin.class)
public class RulesAcceptCommandTest {
    private Command mockCommand;
    private Player mockAllowedPlayer;
    private Player mockDisallowedPlayer;
    private CommandSender mockNonPlayerCommandSender;
    private JavaPlugin mockPlugin;
    private static final String COMMAND_LABEL = "rulesaccept";
    private Server mockServer;
    private RulesAcceptCommand mockRulesAcceptCommand;
    private Config mockConfig;
    private String PREFIX = "&1[ServerUtils]&r";
    private int MAXTIME = 120;
    private World mockWorld;
    private Location mockLocation;

    @Before
    public void setUp() {
        mockPlugin = PowerMockito.mock(JavaPlugin.class);
        mockCommand = mock(Command.class);
        mockServer = mock(Server.class);
        when(mockPlugin.getServer()).thenReturn(mockServer);
        mockAllowedPlayer = mock(Player.class);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.tp")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.set.tp")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.disable.tp")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.enable.tp")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.set.password")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.set.ignorecase")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.get.password")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.set.group.newbie")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.set.group.promote")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.get.group.newbie")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.get.group.promote")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept.reload")).thenReturn(true);
        when(mockAllowedPlayer.hasPermission("serverutils.rulesaccept")).thenReturn(true);
        mockDisallowedPlayer = mock(Player.class);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.tp")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.set.tp")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.disable.tp")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.enable.tp")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.set.password")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.set.ignorecase")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.get.password")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.set.group.newbie")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.set.group.promote")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.get.group.newbie")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.get.group.promote")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept.reload")).thenReturn(false);
        when(mockDisallowedPlayer.hasPermission("serverutils.rulesaccept")).thenReturn(false);
        mockNonPlayerCommandSender = mock(CommandSender.class);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.tp")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.set.tp")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.disable.tp")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.enable.tp")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.set.password")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.set.ignorecase")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.get.password")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.set.group.newbie")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.set.group.promote")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.get.group.newbie")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.get.group.promote")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept.reload")).thenReturn(true);
        when(mockNonPlayerCommandSender.hasPermission("serverutils.rulesaccept")).thenReturn(true);
        mockConfig = mock(Config.class);
        when(mockConfig.getValue(eq("rulesaccept.position.x"))).thenReturn(1.0);
        when(mockConfig.getValue(eq("rulesaccept.position.y"))).thenReturn(1.0);
        when(mockConfig.getValue(eq("rulesaccept.position.z"))).thenReturn(1.0);
        when(mockConfig.getValue(eq("rulesaccept.position.yaw"))).thenReturn(1.0);
        when(mockConfig.getValue(eq("rulesaccept.position.pitch"))).thenReturn(1.0);
        when(mockConfig.getValue(eq("rulesaccept.password"))).thenReturn("Foobar");
        when(mockConfig.getValue(eq("rulesaccept.group.newbie"))).thenReturn("default");
        when(mockConfig.getValue(eq("rulesaccept.group.promote"))).thenReturn("Spieler");
        mockRulesAcceptCommand = new RulesAcceptCommand(mockPlugin, mockConfig);
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        mockWorld = mock(World.class);
        when(mockServer.getWorld(anyString())).thenReturn(mockWorld);
        mockLocation = mock(Location.class);
        when(mockLocation.getX()).thenReturn(1.0);
        when(mockLocation.getY()).thenReturn(1.0);
        when(mockLocation.getZ()).thenReturn(1.0);
        when(mockLocation.getYaw()).thenReturn(1.0f);
        when(mockLocation.getPitch()).thenReturn(1.0f);
        when(mockLocation.getWorld()).thenReturn(mockWorld);
        when(mockAllowedPlayer.getLocation()).thenReturn(mockLocation);
    }

    @Test
    public void testCommandCase1() throws Exception {
        String args[] = new String[] {"tp"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"tp", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(1)).getValue(eq("rulesaccept.position.x"));
        verify(mockConfig, times(1)).getValue(eq("rulesaccept.position.y"));
        verify(mockConfig, times(1)).getValue(eq("rulesaccept.position.z"));
        verify(mockConfig, times(1)).getValue(eq("rulesaccept.position.world"));
        verify(mockConfig, times(1)).getValue(eq("rulesaccept.position.yaw"));
        verify(mockConfig, times(1)).getValue(eq("rulesaccept.position.pitch"));
        verify(mockAllowedPlayer, times(1)).teleport((Location) anyObject());
        verify(mockDisallowedPlayer, never()).teleport((Location) anyObject());
    }

    @Test
    public void testCommandCase2() throws Exception {
        String args[] = new String[] {"settp"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"settp", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(1)).setValue(eq("rulesaccept.position.x"), anyObject());
        verify(mockConfig, times(1)).setValue(eq("rulesaccept.position.y"), anyObject());
        verify(mockConfig, times(1)).setValue(eq("rulesaccept.position.z"), anyObject());
        verify(mockConfig, times(1)).setValue(eq("rulesaccept.position.world"), anyObject());
        verify(mockConfig, times(1)).setValue(eq("rulesaccept.position.yaw"), anyObject());
        verify(mockConfig, times(1)).setValue(eq("rulesaccept.position.pitch"), anyObject());
        verify(mockAllowedPlayer, times(1)).getLocation();
    }

    @Test
    public void testCommandCase3() throws Exception {
        String args[] = new String[] {"disabletp"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"disabletp", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).setValue(eq("rulesaccept.tp.enable"), eq(false));
    }

    @Test
    public void testCommandCase4() throws Exception {
        String args[] = new String[] {"enabletp"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"enabletp", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).setValue(eq("rulesaccept.tp.enable"), eq(true));
    }

    @Test
    public void testCommandCase5() throws Exception {
        String args[] = new String[] {"ignorecase"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"ignorecase", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).setValue(eq("rulesaccept.ignoreCase"), eq(true));
    }

    @Test
    public void testCommandCase6() throws Exception {
        String args[] = new String[] {"disableignorecase"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"disableignorecase", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).setValue(eq("rulesaccept.ignoreCase"), eq(false));
    }

    @Test
    public void testCommandCase7() throws Exception {
        String args[] = new String[] {"getpassword"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"getpassword", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).getValue(eq("rulesaccept.password"));
    }

    @Test
    public void testCommandCase8() throws Exception {
        String args[] = new String[] {"setpassword"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"setpassword", "something"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).setValue(eq("rulesaccept.password"), eq(args[1]));
    }

    @Test
    public void testCommandCase9() throws Exception {
        String args[] = new String[] {"getnewbiegroup"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"getnewbiegroup", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).getValue(eq("rulesaccept.group.newbie"));
    }

    @Test
    public void testCommandCase10() throws Exception {
        String args[] = new String[] {"setnewbiegroup"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"setnewbiegroup", "something"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).setValue(eq("rulesaccept.group.newbie"), eq(args[1]));
    }

    @Test
    public void testCommandCase11() throws Exception {
        String args[] = new String[] {"getpromotegroup"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"getpromotegroup", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).getValue(eq("rulesaccept.group.promote"));
    }

    @Test
    public void testCommandCase12() throws Exception {
        String args[] = new String[] {"setpromotegroup"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"setpromotegroup", "something"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).setValue(eq("rulesaccept.group.promote"), eq(args[1]));
    }

    @Test
    public void testCommandCase13() throws Exception {
        String args[] = new String[] {"reload"};
        assertTrue(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"reload", "something"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        verify(mockConfig, times(2)).loadConfig();
    }

    public void testCommandCase14() throws Exception {
        String args[] = new String[] {};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
        args = new String[] {"foobar"};
        assertFalse(mockRulesAcceptCommand.onCommand(mockAllowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertTrue(mockRulesAcceptCommand.onCommand(mockDisallowedPlayer, mockCommand, COMMAND_LABEL, args));
        assertFalse(mockRulesAcceptCommand.onCommand(mockNonPlayerCommandSender, mockCommand, COMMAND_LABEL, args));
    }
}