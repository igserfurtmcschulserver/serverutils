package de.cag_igs.serverutils.commands;

import de.cag_igs.serverutils.data.Config;
import de.cag_igs.serverutils.data.broadcaster.BroadcasterDatabaseManager;
import de.cag_igs.serverutils.schedulers.Broadcaster;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest(JavaPlugin.class)
public class BroadcasterCommandTest {

    private Command mockCommand;
    private CommandSender mockAllowedCommandSender;
    private CommandSender mockDisallowedCommandSender;
    private JavaPlugin mockPlugin;
    private static final String COMMAND_LABEL = "broadcaster";
    private Server mockServer;
    private BroadcasterCommand broadcasterCommand;
    private Config mockConfig;
    private String PREFIX = "&1[ServerUtils]&r";
    private int MAXTIME = 120;
    private BroadcasterDatabaseManager mockBroadcasterDatabaseManager;
    private Broadcaster mockBroadcaster;


    private String[] permute(String s) {
        String[] returnArray;
        if(s.length() == 1){
            returnArray = new String[2];
            returnArray[0] = s.toUpperCase();
            returnArray[1] = s.toLowerCase();
            return returnArray;
        }
        String[] permutedArray = permute(s.substring(1));
        returnArray = new String[permutedArray.length*2];
        for(int i = 0; i < permutedArray.length; i++){
            returnArray[i*2] = s.substring(0,1).toUpperCase()+permutedArray[i];
            returnArray[i*2+1] = s.substring(0,1).toLowerCase()+permutedArray[i];
        }
        return returnArray;
    }

    private int getRandomNumber(int max, int min) {
        return min + Math.abs(new Random().nextInt() % (max - min));
    }

    @Before
    public void setUp() {
        mockPlugin = PowerMockito.mock(JavaPlugin.class);
        mockCommand = mock(Command.class);
        mockServer = mock(Server.class);
        when(mockPlugin.getServer()).thenReturn(mockServer);
        mockAllowedCommandSender = mock(CommandSender.class);
        when(mockAllowedCommandSender.hasPermission(eq("serverutils.broadcaster.add"))).thenReturn(true);
        when(mockAllowedCommandSender.hasPermission(eq("serverutils.broadcaster.remove"))).thenReturn(true);
        when(mockAllowedCommandSender.hasPermission(eq("serverutils.broadcaster.toggle"))).thenReturn(true);
        when(mockAllowedCommandSender.hasPermission(eq("serverutils.broadcaster.reload"))).thenReturn(true);
        mockDisallowedCommandSender = mock(CommandSender.class);
        when(mockDisallowedCommandSender.hasPermission(eq("serverutils.broadcaster.add"))).thenReturn(false);
        when(mockDisallowedCommandSender.hasPermission(eq("serverutils.broadcaster.remove"))).thenReturn(false);
        when(mockDisallowedCommandSender.hasPermission(eq("serverutils.broadcaster.toggle"))).thenReturn(false);
        when(mockDisallowedCommandSender.hasPermission(eq("serverutils.broadcaster.reload"))).thenReturn(false);
        mockConfig = mock(Config.class);
        mockBroadcasterDatabaseManager = mock(BroadcasterDatabaseManager.class);
        when(mockConfig.getValue(eq("broadcaster.prefix"))).thenReturn(PREFIX);
        when(mockConfig.getValue(eq("broadcaster.maxtime"))).thenReturn(MAXTIME);
        mockBroadcaster = mock(Broadcaster.class);
        broadcasterCommand = new BroadcasterCommand(mockPlugin, mockConfig, mockBroadcasterDatabaseManager,
                mockBroadcaster);
    }

    @Test
    public void testAddInOnCommandTestCase1() {
        String[] message = new String[]{"subPlaceholder", "timePlaceholder", "This", " is ", "a", " simple ",
                "message!"};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        message[0] = "new";
        message[1] = Integer.toString(getRandomNumber(MAXTIME, 1));
        assertTrue(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL, message));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL, message));
        message[0] = "add";
        assertTrue(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL, message));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL, message));
        verify(mockAllowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockDisallowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockBroadcasterDatabaseManager, times(2)).insertEntry(anyInt(), anyString(), anyBoolean());
    }

    @Test
    public void testAddInOnCommandTestCase2() {
        String[] message = new String[]{"subPlaceholder", "timePlaceholder"};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        message[0] = "new";
        message[1] = Integer.toString(getRandomNumber(MAXTIME, 1));
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL, message));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL, message));
        message[0] = "add";
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL, message));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL, message));
        verify(mockAllowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockDisallowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockBroadcasterDatabaseManager, never()).insertEntry(anyInt(), anyString(), anyBoolean());
    }

    @Test
    public void testAddInOnCommandTestCase3() {
        String[] message = new String[]{"subPlaceholder", "timePlaceholder", "This", " is ", "a", " simple ",
                "message!"};
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        message[0] = "new";
        message[1] = Integer.toString(getRandomNumber(900570302, MAXTIME));
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL, message));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL, message));
        message[0] = "add";
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL, message));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL, message));
        verify(mockAllowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockDisallowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockBroadcasterDatabaseManager, never()).insertEntry(anyInt(), anyString(), anyBoolean());
    }

    @Test
    public void testRemoveInOnCommandTestCase1() {
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"remove", Integer.toString(getRandomNumber(3234, 1))}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"remove", Integer.toString(getRandomNumber(3234, 1))}));
        assertTrue(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"delete", Integer.toString(getRandomNumber(3234, 1))}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"delete", Integer.toString(getRandomNumber(3234, 1))}));
        verify(mockAllowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockDisallowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockBroadcasterDatabaseManager, times(2)).deleteEntryById(anyInt());
    }

    @Test
    public void testRemoveInOnCommandTestCase2() {
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"remove", "foobar"}));
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"remove"}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"remove", "foobar"}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"remove"}));
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"delete", "foobar"}));
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"delete"}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"delete", "foobar"}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"delete"}));
        verify(mockAllowedCommandSender, times(4)).sendMessage(anyString());
        verify(mockDisallowedCommandSender, times(4)).sendMessage(anyString());
        verify(mockBroadcasterDatabaseManager, never()).deleteEntryById(anyInt());
    }

    @Test
    public void testToggleInOnCommandTestCase1() {
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"toggle", Integer.toString(getRandomNumber(3234, 1))}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"toggle", Integer.toString(getRandomNumber(3234, 1))}));
        verify(mockAllowedCommandSender).sendMessage(anyString());
        verify(mockDisallowedCommandSender).sendMessage(anyString());
        verify(mockBroadcasterDatabaseManager).updateToggleStateById(anyInt());
    }

    @Test
     public void testToggleInOnCommandTestCase2() {
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"toggle", "foobar"}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"toggle", "foobar"}));
        verify(mockAllowedCommandSender).sendMessage(anyString());
        verify(mockDisallowedCommandSender).sendMessage(anyString());
        verify(mockBroadcasterDatabaseManager, never()).updateToggleStateById(anyInt());
    }

    @Test
    public void testToggleInOnCommandTestCase3() {
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"toggle", "2123", "barfoo"}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"toggle", "3213", "barfoo"}));
        verify(mockAllowedCommandSender).sendMessage(anyString());
        verify(mockDisallowedCommandSender).sendMessage(anyString());
        verify(mockBroadcasterDatabaseManager, never()).updateToggleStateById(anyInt());
    }

    @Test
    public void testReloadInOnCommandTestCase1() {
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertTrue(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"reload"}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"reload"}));
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"reload", Integer.toString(getRandomNumber(3234, 1))}));
        assertTrue(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{"reload", Integer.toString(getRandomNumber(3234, 1))}));
        verify(mockAllowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockDisallowedCommandSender, times(2)).sendMessage(anyString());
        verify(mockBroadcaster).reloadBroadcasts();
    }

    @Test
    public void testEmptyOnCommandTestCase1() {
        when(mockCommand.getName()).thenReturn(COMMAND_LABEL);
        assertFalse(broadcasterCommand.onCommand(mockAllowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{Integer.toString(getRandomNumber(3234, 1))}));
        assertFalse(broadcasterCommand.onCommand(mockDisallowedCommandSender, mockCommand, COMMAND_LABEL,
                new String[]{Integer.toString(getRandomNumber(3234, 1))}));
        verify(mockAllowedCommandSender).sendMessage(anyString());
        verify(mockDisallowedCommandSender).sendMessage(anyString());
    }

    //TODO: add Test for /broadcaster list [page]

    @Test
    public void testListOnCommandTestCase1() {

    }
}